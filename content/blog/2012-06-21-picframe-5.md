---
title: PicFrame 5
author: David

date: 2012-06-21
url: /2012/06/picframe-5/
categories:
  - Apps
tags:
  - PicFrame
  - Update
---
<p><img src="/images//2012/06/Image.jpg" alt="PicFrame 5" title="PicFrame 5" width="500" height="500" class="aligncenter size-full wp-image-622" /><br />
<a href="/picframe/" title="PicFrame">PicFrame</a> for iOS now supports up to 9 photos in a single frame!  We have also added free text labels, 7 extra frames and 3 new effects.  Check it out in the <a href="http://itunes.apple.com/app/id433398108?mt=8">App Store</a>.</p>
