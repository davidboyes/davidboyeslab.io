---
title: PicFrame for iOS Updated to v7
author: David

date: 2013-05-01
url: /2013/05/picframe-for-ios-updated-to-v7/
categories:
  - Apps
tags:
  - PicFrame
  - Update
---
<p><img src="/images//2013/05/picframe72.png" alt="PicFrame 7" width="500" height="512" class="aligncenter size-full wp-image-788" /></p>
<p><a href="/picframe/" title="PicFrame">PicFrame</a> for iOS has been updated in the App Store! It is now much faster to fill your PicFrame! Speed improvements and you can select multiple photos at once!</p>
<p>Download it in the <a href="http://appstore.com/activedevelopment/picframe">App Store</a> now.</p>
