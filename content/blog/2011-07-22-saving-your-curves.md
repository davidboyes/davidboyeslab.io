---
title: Saving your curves
author: David

date: 2011-07-22
url: /2011/07/saving-your-curves/
categories:
  - Apps
tags:
  - PicTools
---
<p><img class="aligncenter size-full wp-image-139" title="PicTools save curve" src="/images//2011/07/photo.jpg" alt="PicTools save curve" width="536" height="285" /></p>
<p>PicTools now has the ability to save your own curves, after I finish updating a few other things I will submit this to the App Store.  It generally takes around a week for an update to get approved &#8211; although a <a title="Picfx" href="/picfx/">Picfx</a> update was once accepted over night so fingers crossed.</p>
<p>Any curves that are saved appear after the built in presets and will also be used in the Quick function, for random effects.</p>
