---
title: Picfx 2.1 Update
author: David

date: 2011-10-04
url: /2011/10/picfx-2-1-update/
categories:
  - Apps
tags:
  - Picfx
  - Update
---
<p><a href="/images//2011/10/free-advert.jpg"><img class="aligncenter size-full wp-image-281" title="Picfx Update" src="/images//2011/10/free-advert.jpg" alt="" width="500" height="500" /></a><br />
Picfx has been updated with new effects, the ability to share to Instagram and more! It is also free for a limited time only so check it out <a href="http://itunes.apple.com/app/id417563413?mt=8">here</a>.</p>
<p>Full Update Text:</p>
<ul>
<li>10 new Effects</li>
<li>Faster at applying effects</li>
<li>Share to Instagram</li>
<li>Share to other apps</li>
<li>Share from other apps to Picfx</li>
<li>Bug fixes</li>
</ul>
