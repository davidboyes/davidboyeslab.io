---
title: Picfx Update
author: David

date: 2015-01-15
url: /2015/01/picfx-update/
categories:
  - Apps
tags:
  - Picfx
  - Update
---
<p><img src="/images//2015/01/Picfx_6.5_Update.jpg" alt="Picfx 6.5 Update" width="500" height="500" class="aligncenter size-full wp-image-898" srcset="/images//2015/01/Picfx_6.5_Update.jpg 1000w, /images//2015/01/Picfx_6.5_Update-150x150.jpg 150w, /images//2015/01/Picfx_6.5_Update-300x300.jpg 300w" sizes="(max-width: 500px) 100vw, 500px" /></p>
<p>Picfx has been updated to version 6.5 and now allows you to edit directly in the Photos app. To do so open the Photos app and find a photo you would like to edit with Picfx. Tap the photo and then tap &#8220;Edit&#8221; in the top right, after that tap the three dots in a circle and select Picfx. If you decide you want to remove the Picfx filter later you can revert the changes and go back to the original photo.</p>
<p>Included in this update are three new effects, PFX 27, PFX 28 and PFX 41, great for those gloomy winter photos.</p>
<p>&nbsp;</p>
