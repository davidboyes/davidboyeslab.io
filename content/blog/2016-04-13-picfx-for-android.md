---
title: Picfx for Android
author: David

date: 2016-04-12
url: /2016/04/picfx-for-android/
categories:
  - Apps
tags:
  - Android
  - Picfx
---
<p><iframe width="500" height="281" src="https://www.youtube.com/embed/pc7sjTMXOjk?feature=oembed" frameborder="0" allowfullscreen></iframe></p>
<p>Picfx is now on Android!<br />
Picfx provides you with a vast and growing 130 layerable effects for your mobile photography. With our Add Another function this number becomes infinite with the ability to layer as many different effects, textures, light leaks, bokeh and frames as you like, to create a truly one-of-a-kind photo.</p>
