---
title: PicTools + PicFrame
author: David

date: 2011-08-17
url: /2011/08/pictools-picframe/
categories:
  - Pleasentries
tags:
  - Photos
  - PicFrame
  - PicTools
---
<p><a href="/images//2011/08/photo.jpg"><img class="aligncenter size-full wp-image-185" title="Tree and Moon" src="/images//2011/08/photo.jpg" alt="Tree and Moon" width="500" height="500" /></a></p>
<p>Just some photos of the moon rising, take with the iPhone 4, edited with PicTools and arranged with PicFrame.</p>
