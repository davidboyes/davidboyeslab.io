---
title: PicTools Update Released
author: David

date: 2011-08-01
url: /2011/08/pictools-update-released/
categories:
  - Apps
tags:
  - PicTools
---
<p>The App Store has just put up the latest release of PicTools, the biggest addition is being able to save your own curves. Here is the full list of changes:</p>
<ul>
<li>New Effects</li>
<li>New Overlays</li>
<li>New Border</li>
<li>Save up to 5 custom RGB curve settings</li>
<li>Overlays, Effects and Borders have now been given names for easy reference</li>
<li>Borders now have an opacity control</li>
<li>Interface tweaks</li>
<li>Setting to have a plain background rather than the grid</li>
</ul>
<p>Check it out here <a title="PicTools" href="/pictools/">/pictools</a></p>
