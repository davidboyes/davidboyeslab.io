---
title: Picfx 7th Top Paid App
author: David

date: 2014-06-26
url: /2014/06/picfx-7th-top-paid-app/
categories:
  - Apps
tags:
  - Picfx
---
<p><img class="aligncenter size-full wp-image-891" src="/images//2014/08/Screen-Shot-2014-06-25-at-10-19-15-pm.png" alt="Picfx 7th in app store" width="500" srcset="/images//2014/08/Screen-Shot-2014-06-25-at-10-19-15-pm.png 900w, /images//2014/08/Screen-Shot-2014-06-25-at-10-19-15-pm-300x84.png 300w" sizes="(max-width: 900px) 100vw, 900px" /></p>
<p>Picfx suddenly shot up the iTunes charts in the US this week, at one point making it the 7th Top Paid app on the App Store!  This was a nice surprise, I think the highest it had been before was around 15th &#8211; which we were very happy with at the time but now we have a new record. One day it will reach the top spot!</p>
