---
title: PicFrame For Mac 1.2
author: David

date: 2012-01-06
url: /2012/01/picframe-for-mac-1-2/
categories:
  - Apps
tags:
  - PicFrame For Mac
---
<p>A small update for <a title="PicFrame For Mac" href="/picframe-for-mac/">PicFrame For Mac</a> is out today, a bunch of new patterns have been added and also a bug has been fixed that would occasionally cause images that had been dragged and dropped in to be lower quality.</p>
