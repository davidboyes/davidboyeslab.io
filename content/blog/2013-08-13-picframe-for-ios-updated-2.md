---
title: PicFrame for iOS Updated
author: David

date: 2013-08-13
url: /2013/08/picframe-for-ios-updated-2/
categories:
  - Apps
tags:
  - PicFrame
  - Update
---
<p><a href="/images//2013/08/picframesharingtopath.jpg"><img src="/images//2013/08/picframesharingtopath.jpg" alt="PicFrame sharing options" width="500" height="500" class="aligncenter size-full wp-image-833" srcset="/images//2013/08/picframesharingtopath.jpg 500w, /images//2013/08/picframesharingtopath-150x150.jpg 150w, /images//2013/08/picframesharingtopath-300x300.jpg 300w" sizes="(max-width: 500px) 100vw, 500px" /></a></p>
<p><a href="/picframe/" title="PicFrame">PicFrame</a> for iOS has been updated to version 7.2.  There are two new frames, twelve new patterns and the ability to share directly to Path.</p>
<p>Check it out on the <a href="http://tw.appstore.com/Ub4">App Store</a>.</p>
