---
title: PicTools Update Progress
author: David

date: 2011-07-20
url: /2011/07/pictools-update-progress/
categories:
  - Apps
tags:
  - PicTools
---
<p style="text-align: center;"><img class="size-full wp-image-135 aligncenter" title="Save button" src="/images//2011/07/photo-6.jpg" alt="Save button on curves" width="410" height="308" /></p>
<p>Today I&#8217;m working on an update for PicTools, the main functionality added will be the ability to save your own curves (and possibly overlays) so you can reuse them easily later on.  I&#8217;m planning to allow for 5 custom slots, which you can save to.  These will be displayed along side the usual presets.  Hopefully it won&#8217;t be too long before this update is in the App Store.</p>
