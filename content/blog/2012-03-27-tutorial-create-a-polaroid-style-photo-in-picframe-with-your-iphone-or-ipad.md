---
title: 'Tutorial: Create a Polaroid style photo in PicFrame with your iPhone or iPad'
author: David

date: 2012-03-26
url: /2012/03/tutorial-create-a-polaroid-style-photo-in-picframe-with-your-iphone-or-ipad/
categories:
  - Tutorial
tags:
  - PicFrame
---
<p>You can easily create a Polaroid style photo in <a title="PicFrame" href="/picframe/">PicFrame</a>, and with the labels in app purchase add some text to complete the look.</p>
<p>To start with select the frame with two areas for photos.  One at the top, the other at the bottom.</p>
<p><a href="/images//2012/02/photo-1.png"><img class="aligncenter size-full wp-image-410" title="Main PicFrame Screen" src="/images//2012/02/photo-1.png" alt="Main PicFrame Screen" width="300" height="450" /></a></p>
<p>Insert a photo into the top area and drag the border down so there is only a small area at the bottom. I&#8217;ve also rounded the corners slightly.</p>
<p><a href="/images//2012/02/photo-2.png"><img class="aligncenter size-full wp-image-411" title="Photo Adjust Screen" src="/images//2012/02/photo-2.png" alt="Photo Adjust Screen" width="300" height="450" /></a></p>
<p>In the Style screen make the border width slight larger and select the pattern that is highlighted with a blue border in the screenshot below.</p>
<p><a href="/images//2012/02/photo-3.png"><img class="aligncenter size-full wp-image-407" title="Pattern Screen" src="/images//2012/02/photo-3.png" alt="Pattern Screen" width="300" height="450" /></a></p>
<p>Finally, if you have the Labels add on for PicFrame you can go to the Labels screen and add one to the bottom area.  I set the label color to be slightly off white and about 70% opacity, with the &#8220;Notethis&#8221; font in an almost black color.  Feel free to play around with different fonts and colors to find something you like.</p>
<p><a href="/images//2012/02/photo-4.png"><img class="aligncenter size-full wp-image-408" title="Label Screen" src="/images//2012/02/photo-4.png" alt="Label Screen" width="300" height="450" /></a></p>
<p>That&#8217;s it!  Save it.  You have created an awesome looking Polaroid style photo.</p>
<p><a href="/images//2012/02/photo-51.jpg"><img class="aligncenter size-full wp-image-414" title="Final Polaroid" src="/images//2012/02/photo-51.jpg" alt="Final Polaroid" width="500" height="500" /></a></p>
