---
title: Picfx v7 for iOS
author: David

date: 2016-09-19
url: /2016/09/picfx-v7-for-ios/
categories:
  - Apps
tags:
  - iOS
  - Picfx
  - Update
---
<p><img class="aligncenter size-full wp-image-939" src="/images//2016/09/tumblr_odlzv0TsoQ1qhlzx0o1_1280.jpg" alt="Picfx v7 graphic" srcset="/images//2016/09/tumblr_odlzv0TsoQ1qhlzx0o1_1280.jpg 1000w, /images//2016/09/tumblr_odlzv0TsoQ1qhlzx0o1_1280-150x150.jpg 150w, /images//2016/09/tumblr_odlzv0TsoQ1qhlzx0o1_1280-300x300.jpg 300w, /images//2016/09/tumblr_odlzv0TsoQ1qhlzx0o1_1280-768x768.jpg 768w" sizes="(max-width: 1000px) 100vw, 1000px" /></p>
<p>Picfx has received a huge update for our version 7 release and is available now in the <a href="http://itunes.apple.com/app/id417563413?mt=8&amp;uo=8&amp;at=10lqbA">App Store</a>. With intuitive interface enhancements, faster image processing, new filters, new textures, new light leaks, filter tweaks and new premium content.</p>
<p><!--more--><br />
**Interface Updates**</p>
<p>Filter strength control is now more accessible<br />
Tap and hold photo to see without filter<br />
Tap filter once to preview, and again to remove preview<br />
Menu with settings and additional information<br />
Icons redesigned</p>
<p>**Filter Updates**</p>
<p>Classics (12 filters)<br />
&#8211;<br />
Marshmallow: Updated colour profile<br />
Candyfloss: New filter<br />
Dawson: New filter<br />
Twilight: New filter modified from Sheen<br />
Autumn: New filter modified from Lana<br />
Fern: Updated colour profile<br />
Meadow: Updated colour profile<br />
Lush: Updated colour profile<br />
Breeze: New filter modified from Olden<br />
Old School: Updated colour profile<br />
Frost: New filter modified from Grit<br />
Sepia: Updated colour profile</p>
<p>PFX Film (25 filters) No changes</p>
<p>Vintage (11 filters)<br />
&#8211;<br />
Dawn: New filter<br />
Dusk: New filter<br />
Valentine: Updated colour profile<br />
Film: New filter<br />
Film 2: New filter<br />
Pola: New filter<br />
Lomo: New filter<br />
Weekend: New filter<br />
Worn: New filter<br />
Haiku: Updated colour profile<br />
Retro: Updated colour profile</p>
<p>Urban (11 filters)<br />
&#8211;<br />
Enhance: No change<br />
Street: New filter<br />
District: New filter<br />
Park: New filter<br />
Neighborhood: New filter<br />
Police: Updated colour profile<br />
Metro: New filter modified from Crime Scene<br />
Downtown: New filter Modified from Alleyway<br />
Suburb: New filter<br />
Police: Updated colour profile<br />
Haze: Updated colour profile<br />
Brick: New filter modified from Red District</p>
<p>Scratches (10 filters)<br />
&#8211;<br />
Scratches 1: New filter<br />
Scratches 2: New filter<br />
Scratches 3: New filter<br />
Scratches 4: New filter<br />
Scratches 5: New filter<br />
Scratches 6: Created from Olden texture<br />
Scratches 7: Created from Sheen texture<br />
Scratches 8:Created from Grit texture<br />
Scratches 9: Created from old Scratches 1<br />
Scratches 10: Created from old Scratches 2</p>
<p>Light (8 filters)<br />
&#8211;<br />
Leak 1: New filter<br />
Leak 2: New filter<br />
Leak 3: New filter<br />
Leak 4: New filter<br />
Leak 5: Modified from Leak 3<br />
Leak 6: Modified from Leak 4<br />
Tutti: No change<br />
Frutti: No change</p>
<p>Bokeh (10 filters) No Changes<br />
Textures (9 textures) No Changes<br />
Space (4 textures) No Changes<br />
Grunge (5 textures) No Changes<br />
Frames(13 frames) No Changes</p>
<p>**New Premium filter sets**</p>
<p>Instant Film (14 filters):<br />
Instant 1: New filter<br />
Instant 2: New filter<br />
Instant 3: New filter<br />
Instant 4: New filter<br />
Instant 5: New filter<br />
Instant 6: New filter<br />
Instant 7: New filter<br />
Instant 8: New filter<br />
Instant Leak 1: New filter<br />
Instant Leak 2: New filter<br />
Instant Leak 3: New filter<br />
Instant Leak 4: New filter<br />
Instant Leak 5: New filter<br />
Instant Leak 6: New filter</p>
<p>BW (10 filters)<br />
BW 1: New filter! FREE<br />
BW 2: New filter! FREE<br />
BW 3: New filter!<br />
BW 4: New filter!<br />
BW 5: New filter!<br />
BW 6: New filter!<br />
BW 7: New filter!<br />
BW 8: New filter!<br />
BW 9: New filter!<br />
BW Grain: New BW grain texture</p>
