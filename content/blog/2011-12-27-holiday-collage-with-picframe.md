---
title: Holiday Collage With PicFrame
author: David

date: 2011-12-26
url: /2011/12/holiday-collage-with-picframe/
categories:
  - Apps
tags:
  - Holiday
  - PicFrame
---
<p><a href="/images//2011/12/photo.jpg"><img class="aligncenter size-full wp-image-382" title="Holiday Beach" src="/images//2011/12/photo.jpg" alt="Beach" width="500" height="500" /></a></p>
<p>Creating a collage with <a title="PicFrame" href="/picframe/">PicFrame</a> of your holiday snaps couldn&#8217;t be easier.  Simply purchase PicFrame from the <a href="http://itunes.apple.com/app/id433398108?mt=8">App Store</a> if you don&#8217;t already have it, pick a frame and add in your photos.  You can also apply effects by double tapping a photo, selecting &#8220;Fx&#8221; then choosing your desired effect.  The above PicFrame photo uses Aqua for the left photo and Classic on the right.</p>
<p>There are much more features in PicFrame which you can use to show off your creative side, check out the rest of them <a title="PicFrame" href="/picframe/">here</a>.</p>
<p>Happy Holidays!</p>
