---
title: Quick Straight
author: David

date: 2013-04-10
url: /2013/04/quick-straight-free/
categories:
  - Apps
tags:
  - Quick Straight
---
<p><img src="/images//2013/04/quickstraight1.jpg" alt="Quick Straight Screenshots" width="500" height="510" class="aligncenter size-full wp-image-773" /><br />
<a href="/quick-straight/" title="Quick Straight">Quick Straight</a> is a simple and easy to use app to help you straighten photos that have uneven horizons. Open the app and it immediately loads your latest photo to straighten. Use two fingers to rotate the photo, or if you want precision drag one finger up or down to rotate it in smaller increments.</p>
<p>Quick Straight is available for free in the <a href="https://itunes.apple.com/app/quick-straight/id629926534?mt=8">App Store</a>.</p>
