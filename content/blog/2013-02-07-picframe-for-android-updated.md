---
title: PicFrame For Android Updated
author: David

date: 2013-02-06
url: /2013/02/picframe-for-android-updated/
categories:
  - Apps
tags:
  - PicFrame For Android
  - Update
---
<p><img class="aligncenter size-full wp-image-725" alt="PicFrame 2 Android" src="/images//2013/02/17201312276.jpg" width="500" height="500" /></p>
<p><a title="PicFrame For Android" href="/picframe-for-android/">PicFrame for Android</a> has been updated to version 2. It now includes shapes so you can change the shape of each photo to a circle, heart, hexagon and more.  We have also added a 9:16 ratio and updated the interface for usability.</p>
<p>Edit: We have just updated it to version 2.5! Now frames are fully adjustable! Drag the inner borders of a frame to customize.</p>
<p>Check it out in <a href="https://play.google.com/store/apps/details?id=nz.co.activedevelopment.picframe_android">Google Play</a>.</p>
