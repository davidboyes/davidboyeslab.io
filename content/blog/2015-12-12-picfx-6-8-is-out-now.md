---
title: Picfx 6.8 is out now!
author: David

date: 2015-12-11
url: /2015/12/picfx-6-8-is-out-now/
categories:
  - Apps
tags:
  - Picfx
  - Update
---
<p><img src="/images//2015/12/12276770_630996283706384_395487626_n.jpg" alt="Picfx 6.8" width="500" class="aligncenter size-full wp-image-906" srcset="/images//2015/12/12276770_630996283706384_395487626_n.jpg 750w, /images//2015/12/12276770_630996283706384_395487626_n-240x300.jpg 240w" sizes="(max-width: 750px) 100vw, 750px" /></p>
<p>In this update:<br />
• On supported devices you can use the 3D Touch shortcut menu to edit your most recent photo or launch directly into the camera<br />
• Crop screen lets you select a ratio<br />
• Picfx now handles photos that are stored in your iCloud correctly<br />
• Fixed photo thumbnails. Now displaying in full resolution.</p>
<p>Available now in the <a href="http://itunes.apple.com/app/id417563413?mt=8">App Store</a>. </p>
