---
title: Responding To Reviews
author: David

date: 2011-09-03
url: /2011/09/responding-to-reviews/
categories:
  - Apps
tags:
  - Help
  - PicFrame
---
<p>Although it is great that people can write reviews on apps they have tried, I have noticed someone requested a feature that already exists. Perhaps it is a user interface issue that they aren&#8217;t aware of the functionality and I will think about ways to address this.</p>
<p>iTunes reviewer Aybaybaay asks:</p>
<blockquote><p>Could there be an option to not have any borders at all? So that the pictures are actually touching one another</p></blockquote>
<p>To do this, you simple click on the style tab, and then move the first slider left or right to have the size of the border go from non existent to large.</p>
<div id="attachment_205" style="width: 310px" class="wp-caption aligncenter"><a href="/images//2011/09/Photo-3-09-11-4-14-49-PM.png"><img class="size-medium wp-image-205" title="PicFrame Border Example" src="/images//2011/09/Photo-3-09-11-4-14-49-PM-300x297.png" alt="" width="300" height="297" /></a><p class="wp-caption-text">The top slider shows there is a small border</p></div>
<div id="attachment_206" style="width: 310px" class="wp-caption aligncenter"><a href="/images//2011/09/Photo-3-09-11-4-14-53-PM.png"><img class="size-medium wp-image-206" title="PicFrame Borderless Example" src="/images//2011/09/Photo-3-09-11-4-14-53-PM-300x296.png" alt="" width="300" height="296" /></a><p class="wp-caption-text">Sliding it all the way to the left removes the border</p></div>
