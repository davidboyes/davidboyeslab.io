---
title: PicFrame Update
author: David

date: 2011-09-27
url: /2011/09/picframe-update-2/
categories:
  - Apps
tags:
  - PicFrame
  - Update
---
<p><img class="aligncenter size-full wp-image-233" title="PicFrame 2 Update" src="/images//2011/09/picframe-2.jpg" alt="" width="500" height="500" /></p>
<ul>
<li>Double the amount of frames!</li>
<li>Now a Universal app &#8211; iPad &amp; iPhone</li>
<li>Support for up to 5 images in a frame</li>
<li>Four new patterns</li>
<li>Easy border color picker</li>
<li>Added button to clear images &#8211; start fresh.</li>
<li>Send your image to Instagram and other installed apps</li>
<li>More options when saving to Flickr and Tumblr</li>
<li>Fixed saving bug</li>
<li>Fixed others bugs</li>
</ul>
<p>&nbsp;</p>
<p>An optional in app purchase to add labels to the frames to help share your story</p>
<ul>
<li>Preset labels to get you started</li>
<li>Multiple fonts to use</li>
<li>Choose text and label color + transparency</li>
<li>Optional round or torn tape style edges on label</li>
<li>Drag and rotate the label to your desired position</li>
<li>High quality text in final image</li>
</ul>
<div><a href="http://itunes.apple.com/app/id433398108?mt=8">View it in the iTunes App Store</a></div>
<p></p>
