---
title: PicTools Free For One Day
author: David

date: 2011-08-23
url: /2011/08/pictools-free-for-one-day/
categories:
  - Apps
tags:
  - Free
  - PicTools
---
<p>PicTools is free for one day only!  Get in quick, you can download it in the <a href="http://itunes.apple.com/app/pictools/id446664420?mt=8">Apple App Store</a>.</p>
