---
title: Picfx v5 Released
author: David

date: 2012-09-10
url: /2012/09/picfx-v5-released/
categories:
  - Apps
tags:
  - Picfx
  - Update
---
<p><img src="/images//2012/09/PICFX-IG.jpg" alt="Picfx v5 logo" title="Picfx v5" width="500" height="500" class="aligncenter size-full wp-image-670" /><br />
<a href="http://picfx.co">Picfx</a> has been updated and now sits at version 5. We have added more effects including a new set &#8220;PFX Film&#8221;. Along with that we have made it easier to share to Instagram and have tweaked the interface to make layering effects more accessible.  Check it out in the iOS <a href="http://itunes.apple.com/app/id417563413?mt=8">App Store</a>.</p>
