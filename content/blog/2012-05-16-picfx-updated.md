---
title: Picfx Updated
author: David

date: 2012-05-16
url: /2012/05/picfx-updated/
categories:
  - Apps
tags:
  - Picfx
  - Update
---
<p style="text-align: center;"><img class="size-full wp-image-556 aligncenter" title="picfx v4" src="/images//2012/05/picfxv4.jpg" alt="picfx v4" width="500" height="500" /></p>
<p><a href="/picfx">Picfx</a> has been updated to version 4, we now have over 100 effects and the graphics and icon have been updated as well!  Check it out in the iOS <a href="http://itunes.apple.com/app/id417563413?mt=8">App Store</a>.</p>
