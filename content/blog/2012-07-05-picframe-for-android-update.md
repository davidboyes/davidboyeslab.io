---
title: PicFrame For Android Update
author: David

date: 2012-07-05
url: /2012/07/picframe-for-android-update/
categories:
  - Apps
tags:
  - Android
  - PicFrame
  - Update
---
<p><img src="/images//2012/07/652012171658.png" alt="Android PicFrame patterned background" title="Android PicFrame Example" width="500" height="500" class="aligncenter size-full wp-image-636" /></p>
<p>The <a href="/picframe-for-android/" title="PicFrame For Android">Android version of PicFrame</a> has just been updated.  We have added patterned backgrounds and have tweaked the effects dialog for usability along with some bug fixes.</p>
<p>Check it out in the <a href="https://play.google.com/store/apps/details?id=nz.co.activedevelopment.picframe_android">Google Play</a> store.</p>
