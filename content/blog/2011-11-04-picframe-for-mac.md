---
title: PicFrame For Mac
author: David

date: 2011-11-04
url: /2011/11/picframe-for-mac/
categories:
  - Apps
tags:
  - Mac
  - PicFrame
---
<p><a href="/picframe-for-mac/"><img src="/images//2011/11/blog.jpg" alt="" title="blog" width="500" height="439" class="aligncenter size-full wp-image-357" /></a></p>
<p>After receiving quite a bit of interest for a Mac version of PicFrame we are happy to announce that it is now available on the Mac App Store.</p>
<p>PicFrame on the Mac is just as easy to use, select a frame, drag and drop your images in, tweak the border size, give the photos rounded corners, add a color or pattern, resize the adjustable frames and save the photo or share it to Facebook.</p>
<p>More information and a link to purchase can be found <a href="/picframe-for-mac/" title="PicFrame For Mac">here</a>.</p>
