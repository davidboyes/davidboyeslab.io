---
title: Picfx Update 2.2
author: David

date: 2011-10-31
url: /2011/11/picfx-update-2-2/
categories:
  - Uncategorized
---
<p>Due to a bug in the iOS 5 move and scale crop screen we have decided to create our own so we have control of it and hopefully bugs like this won&#8217;t appear through iOS system updates.  The update with the new crop screen has been released today.</p>
<p>More information on Picfx <a href="/picfx/" title="Picfx">here</a></p>
