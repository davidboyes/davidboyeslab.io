---
title: PicTools App Released
author: David
date: 2011-07-17
url: /2011/07/pictools-app-released/
categories:
  - Apps
tags:
  - apps
  - PicTools
---
<p><a href="/images//2011/07/pictools-icon.png"><img class="alignleft size-full wp-image-12" title="pictools-icon" src="/images//2011/07/pictools-icon.png" alt="PicTools Icon" width="82" height="82" /></a><a href="/pictools">PicTools</a> is the latest iPhone app we have in the iOS App Store, it&#8217;s aim is to provide an easy to use interface to edit your photos with. The idea to create this app came from using other advanced iPhone based photo editors that provided a lot of functionality, but had a complicated interface, or weren&#8217;t quick enough if you just wanted to make a few small edits. So PicTools tries to be light weight and quick, but can still do advanced editing and also introduce beginners to things such as RGB curves through the presets.</p>
<p>A <a href="http://appotography.com/2011/07/12/pictools-review-iphone/">review</a> at Appotography on PicTools seemed to capture exactly the above and concludes the review by stating</p>
<blockquote><p>Not only it does give you enough to enhance your photos in a cinch, but it is also very simple and user-friendly and it allows great results, without going through all the trouble of tweaking with massive iPhone photo editors.</p></blockquote>
<p>I recommend reading the review and it also has a couple of great example output images. You can also find out more about PicTools <a href="/pictools">here</a></p>
