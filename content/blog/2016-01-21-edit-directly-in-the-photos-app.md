---
title: Edit directly within the Photos app
author: David

date: 2016-01-21
url: /2016/01/edit-directly-in-the-photos-app/
enclosure:
  - |
    |
        /images//2016/01/img_2678.mov
        3394464
        video/quicktime
        
categories:
  - Apps
  - Tutorial
tags:
  - Picfx
---
<p>Did you know you can use Picfx directly in the Photos app? Here&#8217;s a quick 15 second video showing you how.</p>
<p><video src="/images//2016/01/img_2678.mov" poster="/images//2016/01/FullSizeRender-10.jpg" preload="metadata" controls="controls" width="500"></video></p>
