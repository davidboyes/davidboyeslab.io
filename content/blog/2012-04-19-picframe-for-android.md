---
title: PicFrame For Android
author: David

date: 2012-04-19
url: /2012/04/picframe-for-android/
categories:
  - Apps
tags:
  - Android
  - PicFrame
---
<p><img class="aligncenter size-full wp-image-517" title="PicFrame for Android Promo" src="/images//2012/04/promo_small.png" alt="" width="500" height="244" /></p>
<p><a title="PicFrame For Android" href="/picframe-for-android/">PicFrame</a> is now available on Android devices!  You can download it in the <a href="https://play.google.com/store/apps/details?id=nz.co.activedevelopment.picframe_android">Google Play</a> store.  Testing Android apps is quite a bit different as there is an almost endless list of hardware variation.  We have tested on quite a few popular devices with varying power and screen sizes and it all appears fine, but if your device has any quirks then let us know and we can update it immediately (a bonus with the Android store).</p>
<p>We look forward to bring out great updates to take this app to the next level!</p>
