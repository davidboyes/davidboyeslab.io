---
title: 'Tutorial: Layering effects in Picfx'
author: David

date: 2012-03-08
url: /2012/03/tutorial-layering-effects-in-picfx/
categories:
  - Apps
  - Tutorial
tags:
  - Picfx
---
<p><a href="https://www.facebook.com/notes/picfx/soft-tone-layering-1/335881113130440"><img class="aligncenter size-full wp-image-433" title="Picfx Tutorial" src="/images//2012/03/picfx-tutorial.jpg" alt="Picfx Example" width="500" height="500" /></a></p>
<p>Check out the <a href="https://www.facebook.com/notes/picfx/soft-tone-layering-1/335881113130440">Picfx Facebook page</a> for an excellent tutorial on layering effects to create amazing results.</p>
