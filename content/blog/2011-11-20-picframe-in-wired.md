---
title: PicFrame in Wired
author: David

date: 2011-11-20
url: /2011/11/picframe-in-wired/
categories:
  - Uncategorized
---
<p><img src="/images//2011/11/wired-app-guide.jpg" alt="Wired App Guide" title="Wired App Guide" width="500" height="667" class="aligncenter size-full wp-image-365" /><br />
Wired recently released an App Guide which catalogs and reviews 400 different apps over the iOS, Android and Windows Mobile platforms.  PicFrame was included and received a 9/10.</p>
<p>If you look closely enough you can see the PicFrame icon on the front cover &#8211; hint: it is shaded blue.</p>
