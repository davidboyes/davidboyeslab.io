---
title: PicTools Update 1.50
author: David

date: 2011-09-28
url: /2011/09/pictools-update-1-50/
categories:
  - Apps
tags:
  - PicTools
  - Update
---
<p>An update for PicTools has hit the App Store and transforms the App into a Universal app so that it now works natively on the iPad as well as the iPhone.</p>
<p><a href="http://itunes.apple.com/app/id446664420?mt=8">Check it out here</a></p>
