---
title: PicFrame For Mac 1.4
author: David

date: 2012-05-16
url: /2012/05/picframe-for-mac-1-4/
categories:
  - Apps
tags:
  - PicFrame For Mac
  - Update
---
<p><img class="aligncenter size-full wp-image-549" title="PicFrame Mac" src="/images//2012/05/blog.jpg" alt="" width="500" height="439" /></p>
<p><a title="PicFrame For Mac" href="/picframe-for-mac/">PicFrame for Mac</a> has been updated to version 1.4.  In this update you will find a bug fix that had previously caused images to incorrectly be zoomed in, a higher resolution output and we have added 16:9 and 2:3 ratio output.</p>
<p>Check it out in the <a href="http://itunes.apple.com/app/id475067528?mt=12">Mac App Store.</a></p>
