---
title: PicBoost and Bokehful Updated
author: David

date: 2013-03-08
url: /2013/03/picboost-and-bokehful-updated/
categories:
  - Apps
tags:
  - Bokehful
  - PicBoost
  - Update
---
<p><a href="/images//2013/03/picboostbokehful.jpg"><img src="/images//2013/03/picboostbokehful.jpg" alt="PicBoost and Bokehful Screenshots" width="500" height="500" class="aligncenter size-full wp-image-741" /></a></p>
<p>A small update for <a href="/picboost/" title="PicBoost">PicBoost</a> and <a href="/bokehful/" title="Bokehful">Bokehful</a> has been release that adds iPhone 5 support to each app.</p>
