---
title: PicFrame for iOS updated to 9.5
author: David

date: 2016-01-25
url: /2016/01/picframe-for-ios-updated-to-9-5/
categories:
  - Apps
tags:
  - PicFrame
  - Update
---
<p><img src="/images//2016/01/IMG_2806.jpg" alt="IMG_2806" width="500" class="aligncenter wp-image-925" srcset="/images//2016/01/IMG_2806.jpg 1080w, /images//2016/01/IMG_2806-240x300.jpg 240w, /images//2016/01/IMG_2806-768x960.jpg 768w, /images//2016/01/IMG_2806-819x1024.jpg 819w" sizes="(max-width: 1080px) 100vw, 1080px" /></p>
<p><a href="http://picframeapp.com">PicFrame</a> has been updated to version 9.5 on iOS and brings with it support for 4:5 and 5:4 ratio frames along with suitable resolutions to create perfect full sized Instagram images.</p>
<p>You can find PicFrame in the <a href="http://itunes.apple.com/app/id433398108?mt=8&#038;at=10lqbA&#038;ct=activedevelopment">App Store</a>.</p>
