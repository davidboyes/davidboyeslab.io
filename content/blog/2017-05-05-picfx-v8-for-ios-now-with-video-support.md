---
title: Picfx v8 for iOS, now with video support!
author: David

date: 2017-05-04
url: /2017/05/picfx-v8-for-ios-now-with-video-support/
categories:
  - Apps
tags:
  - apps
  - iOS
  - Picfx
  - Update
---
<p><img class="aligncenter size-full wp-image-954" src="/images//2017/05/tumblr_opflzq7YAb1qhlzx0o1_1280.jpg" alt="Picfx v8" srcset="/images//2017/05/tumblr_opflzq7YAb1qhlzx0o1_1280.jpg 960w, /images//2017/05/tumblr_opflzq7YAb1qhlzx0o1_1280-300x94.jpg 300w, /images//2017/05/tumblr_opflzq7YAb1qhlzx0o1_1280-768x240.jpg 768w" sizes="(max-width: 960px) 100vw, 960px" />Now edit VIDEOS, time-lapses, slo-mos and loop videos with Picfx filters!</p>
<p>Refine your photo and videos even further with image adjustment tools: Exposure, Contrast, Brightness, Saturation, Vignette, Sharpen, Highlight Save, Shadow Save.</p>
<p>Established in 2011, Picfx has been part of the iPhone photography community for over 6 years. During this time we have seen many advances in the iPhone camera hardware, commanding many powerful new apps to satisfy the growing number of niches within iPhone photography. </p>
<p>With this update we’ve refined our feature set and focussed on our vision for a highly functional, simple, effective tool for editing your photos and videos. </p>
<p>Besides adding new features we also announce the removal of all overlay filters and textures. Specifically Bokeh, Space, Textures, Grunge, Frames, Scratches &amp; Light leaks. For these needs we invite you to try an alternate texture based Photo App in the App Store.</p>
<p>You can find Picfx in the <a href="https://itunes.apple.com/ca/app/picfx/id417563413?mt=8">App Store</a>.</p>
