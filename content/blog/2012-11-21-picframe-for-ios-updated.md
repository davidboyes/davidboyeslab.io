---
title: PicFrame For iOS Updated
author: David

date: 2012-11-20
url: /2012/11/picframe-for-ios-updated/
categories:
  - Apps
tags:
  - PicFrame
  - Update
---
<p><img class="aligncenter size-full wp-image-698" alt="PicFrame v6 Example" src="/images//2012/12/Photo-12-12-12-11-18-40-AM.jpg" width="500" height="500" /></p>
<p><a title="PicFrame" href="/picframe/">PicFrame</a> for iOS has been updated to version 6!</p>
<p>Whats new:</p>
<ul>
<li><span style="line-height: 13px;">Change the shape of your photos to a circle, heart, hexagon and more</span></li>
<li>Take photos from inside PicFrame</li>
<li>Adjusted interface to make effects and rotation/mirror controls more accessible</li>
<li>Added the ability to remove a single photo from the frame</li>
</ul>
<p>&nbsp;</p>
<p>Check it out in the <a href="https://itunes.apple.com/app/picframe/id433398108?mt=8">App Store</a> now!</p>
