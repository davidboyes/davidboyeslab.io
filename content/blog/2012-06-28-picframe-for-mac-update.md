---
title: PicFrame For Mac Update
author: David

date: 2012-06-28
url: /2012/06/picframe-for-mac-update/
categories:
  - Apps
tags:
  - PicFrame For Mac
  - Update
---
<p><img src="/images//2012/06/screenshot-2-large.png" alt="PicFrame For Mac 9 photos" title="PicFrame For Mac" width="500" height="439" class="aligncenter size-full wp-image-630" /><br />
<a href="/picframe-for-mac/" title="PicFrame For Mac">PicFrame For Mac</a> has just been updated to version 2! Now it includes 67 fully adjustable frames (drag the borders between photos just like the iOS version) that support up to 9 photos. We have also added the ability to save your work in progress as a PicFrame Project file. It saves the frame, all your customizations, settings and any loaded photos so you can work on it later &#8211; or save it without photos and use it as a template!</p>
<p>Check it out in the <a href="http://itunes.apple.com/app/picframe/id475067528?mt=12">Mac App Store</a></p>
