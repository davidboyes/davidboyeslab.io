---
title: PicFrame 8.6 for iOS
author: David

date: 2013-12-14
url: /2013/12/picframe-8-6-for-ios/
categories:
  - Apps
tags:
  - PicFrame
  - Update
---
<p><img src="/images//2013/12/picframe_music.jpg" alt="PicFrame 8.5 Music" width="500" height="500" class="aligncenter size-full wp-image-878" srcset="/images//2013/12/picframe_music.jpg 500w, /images//2013/12/picframe_music-150x150.jpg 150w, /images//2013/12/picframe_music-300x300.jpg 300w" sizes="(max-width: 500px) 100vw, 500px" /><br />
<a href="/picframe/" title="PicFrame">PicFrame</a> for iOS has been updated to version 8.6! You can now add music to your frames and save them as videos. We have also added 16 exclusive holiday themed background patterns &#8211; just in time for Christmas!</p>
<p>Download it now in the <a href="http://itunes.apple.com/app/id433398108?mt=8&#038;at=10lqbA&#038;ct=activedevelopment">AppStore</a>.</p>
