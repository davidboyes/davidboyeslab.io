---
title: 'Android Tutorial: Share Multiple Photos To Instagram With PicFrame'
author: David

date: 2012-04-19
url: /2012/04/android-tutorial-share-multiple-photos-to-instagram-with-picframe/
categories:
  - Tutorial
tags:
  - Android
  - Instagram
  - PicFrame
---
<p>Now that <a href="http://instagr.am/">Instagram</a> is available on Android devices you may be wondering how people are uploading multiple photos.  One way is to use our iOS app <a href="/picframe">PicFrame</a> (over 100,000 photos are tagged with #PicFrame on the Instagram network), well now it&#8217;s available on <a title="PicFrame For Android" href="/picframe-for-android/">Android</a> devices and just like its iPhone counterpart you can use it to share directly to Instagram.</p>
<p>First, install PicFrame from <a href="https://play.google.com/store/apps/details?id=nz.co.activedevelopment.picframe_android">Google Play</a>.  Once it is installed open it up and select one of the 32 frames available that suits the amount of photos you want to share.</p>
<p><a href="/images//2012/04/Screenshot_2012-04-19-17-17-21.png"><img class="aligncenter size-medium wp-image-522" title="PicFrame Frame Selection" src="/images//2012/04/Screenshot_2012-04-19-17-17-21-180x300.png" alt="" width="180" height="300" /></a></p>
<p>Load it up with your photos by tapping the frame areas and inserting them.</p>
<p><a href="/images//2012/04/Screenshot_2012-04-19-17-17-43.png"><img class="aligncenter size-medium wp-image-523" title="Screenshot_2012-04-19-17-17-43" src="/images//2012/04/Screenshot_2012-04-19-17-17-43-180x300.png" alt="" width="180" height="300" /></a></p>
<p>Once you have zoomed and aligned your photos tap the Share icon.  You can leave it on the medium resolution as it is perfect for Instagram.</p>
<p><a href="/images//2012/04/Screenshot_2012-04-19-17-17-49.png"><img class="aligncenter size-medium wp-image-524" title="PicFrame Share Dialog" src="/images//2012/04/Screenshot_2012-04-19-17-17-49-180x300.png" alt="" width="180" height="300" /></a></p>
<p>Tap the Share button and you should be presented with a list of applications/services you can use to share your PicFrame with.  If you have installed Instagram then it will show up in the list.</p>
<p><a href="/images//2012/04/Screenshot_2012-04-19-17-17-561.png"><img class="aligncenter size-medium wp-image-526" title="PicFrame Share Applications" src="/images//2012/04/Screenshot_2012-04-19-17-17-561-180x300.png" alt="" width="180" height="300" /></a></p>
<p>Tapping Instagram will load the application and allow you to crop, then share the PicFrame image.  Don&#8217;t forget to tag it with #PicFrame in the caption!</p>
