---
title: PicFrame for Mac Update
author: David

date: 2013-01-09
url: /2013/01/picframe-for-mac-update-2/
categories:
  - Apps
tags:
  - PicFrame For Mac
  - Update
---
<p><img class="aligncenter size-full wp-image-715" alt="PicFrame for Mac 2.6 Screenshot" src="/images//2013/01/Screen-Shot-2013-01-10-at-10.04.39-AM.png" width="500" height="432" /></p>
<p><a title="PicFrame For Mac" href="/picframe-for-mac/">PicFrame for Mac</a> has been updated to version 2.6. We have included four new frames and have also changed to use the built in sharing provided by OS X 10.8 Mountain Lion which means you can share to Facebook, Twitter and Flickr.</p>
<p>Check it out in the <a href="http://itunes.apple.com/app/id475067528?mt=12">Mac App Store</a>.</p>
