---
title: PicFrame for Mac 2.8
author: David

date: 2016-11-11
url: /2016/11/picframe-for-mac-2-8/
categories:
  - Apps
tags:
  - Mac
  - OS X
  - PicFrame
  - Update
---
<p><img class="aligncenter size-full wp-image-948" src="/images//2016/11/devicesscreenshot1.jpg" alt="Screenshot of PicFrame" srcset="/images//2016/11/devicesscreenshot1.jpg 1000w, /images//2016/11/devicesscreenshot1-300x188.jpg 300w, /images//2016/11/devicesscreenshot1-768x480.jpg 768w" sizes="(max-width: 1000px) 100vw, 1000px" /></p>
<p><a href="http://itunes.apple.com/app/id475067528?mt=12&amp;at=10lqbA&amp;ct=activedevelopment">PicFrame for Mac</a> has been updated to 2.8 and now supports the Touch Bar on the new MacBook Pro! The interface has also been slightly tweaked, with an updated toolbar area. The full update list is below.</p>
<ul>
<li>Support for Touch Bar on the new MacBook Pro lets you quickly style or save your PicFrame</li>
<li>Resizing the window is now supported, go full screen!</li>
<li>Redesigned tool bar</li>
<li>Increased sharing resolution</li>
<li>Changed zoom behaviour</li>
<li>Interface improvements</li>
</ul>
