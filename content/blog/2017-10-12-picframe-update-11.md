---
title: PicFrame for iOS v11
author: David

date: 2017-10-12
url: /2017/10/picframe-update-11/
categories:
  - Apps
tags:
  - apps
  - iOS
  - PicFrame
  - Update
---
<p><center><iframe width="360" height="640" src="https://www.youtube.com/embed/rZrn60wo9-o?feature=oembed" frameborder="0" allowfullscreen></iframe></center></p>
<p>PicFrame for iOS has been updated to version 11 and includes some interface improvements to adding text labels as well as integration of stickers. Check out the update in the <a href="http://itunes.apple.com/app/id433398108?mt=8&#038;at=10lqbA&#038;ct=activedevelopment">App Store</a> now!</p>
